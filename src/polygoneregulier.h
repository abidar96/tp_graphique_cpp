#ifndef POLYGONEREGULIER_H
#define POLYGONEREGULIER_H
#include "point.h"
#include "figuregeometrique.h"
#include <gtkmm.h>

class Polygoneregulier : public FigureGeometrique
{
    public:
        Polygoneregulier(const Couleur& couleur,const point& centre,int rayon,int nbCotes);
        void afficher(const Cairo::RefPtr<Cairo::Context> & context) const override;
        int getNbPoints() const;
        const point& getPoint(int indice) const;
    protected:

    private:
       int _nbPoints;
       point* _points;
};

#endif // POLYGONEREGULIER_H
