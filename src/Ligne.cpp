#include "Ligne.h"
#include <iostream>
#include"point.h"
#include"Ligne.h"

using namespace std;
Ligne::Ligne(const Couleur& couleur,const point& p0, const point& p1)
:FigureGeometrique(couleur)
   {
    _p0=p0;
    _p1=p1;
   }

void Ligne::  afficher(const Cairo::RefPtr<Cairo::Context> & context) const
{
    //cout << "Ligne " <<_couleur._r<<"_"<<_couleur._g <<"_" << _couleur._b << " " << _p0._x <<"_"<<_p0._y<<" "<<_p1._x <<"_"<<_p1._y<<endl;
	const Couleur & c = getCouleur();
    context->set_source_rgb(c._r, c._g, c._b);

    const point & p0 = _p0;
    context->move_to(p0._x, p0._y);
    
    const point & p1 = _p1;
    context->line_to(p1._x, p1._y);
    
    context->stroke();
    
   
}



