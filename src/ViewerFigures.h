#ifndef ViewerFigures_H
#define ViewerFigures_H
#include <gtkmm.h>
#include "ZoneDessin.h"



class ViewerFigures  {
	
	private:
		Gtk::Main _kit;
		Gtk::Window _window;
		ZoneDessin zone;
	public :
		ViewerFigures(int argc, char** argv);
		void run();
		
};
#endif // ViewerFigures
