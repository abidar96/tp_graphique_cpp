#include "polygoneregulier.h"
#include <math.h>
#define PI 3.141592
#include<iostream>
using namespace std;


Polygoneregulier::Polygoneregulier(const Couleur& couleur,const point& centre,int rayon,int nbCotes)
:FigureGeometrique(couleur)
{
    _nbPoints=nbCotes;
    _points = new point [nbCotes];
    for(int i=0;i<nbCotes;i++){
    point tmp;

    float  ALPHA = i * PI * 2 / nbCotes;
        tmp._x = rayon * cos(ALPHA) + centre._x;
        tmp._y = rayon * sin(ALPHA) + centre._y;
        _points[i]=tmp;

    }


}

void Polygoneregulier:: afficher(const Cairo::RefPtr<Cairo::Context> & context) const
{
    /*cout << "PolygoneRegulier " <<_couleur._r<<"_"<<_couleur._g <<"_" << _couleur._b ;
  
     for(int i=0;i<_nbPoints;i++){
          cout <<" "<< _points[i]._x <<"_" <<_points[i]._y;
     }*/
    const Couleur & c = getCouleur();
    context->set_source_rgb(c._r, c._g, c._b);

    const point & p1 = _points[_nbPoints-1];
    context->move_to(p1._x, p1._y);

    for (int i=0; i<_nbPoints; ++i) {
        const point & p = _points[i];
        context->line_to(p._x, p._y);
    }

    context->stroke();
}
int Polygoneregulier::getNbPoints() const {
        return _nbPoints;
}

const point& Polygoneregulier::getPoint(int indice) const{

    return _points[indice];

}
