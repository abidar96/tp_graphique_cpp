#include "ZoneDessin.h"
#include "Ligne.h"
#include "point.h"
#include "polygoneregulier.h"


ZoneDessin :: ZoneDessin(){
		_figures.push_back(new Ligne({1,0,0}, {10,10}, point{630,10}));
		_figures.push_back(new Ligne(Couleur{1,0,0}, point{630,10}, point{630,470}));
		_figures.push_back(new Ligne(Couleur{1,0,0}, point{10,470}, point{630,470}));
		_figures.push_back(new Ligne(Couleur{1,0,0}, point{10,10}, point{10,470}));
		_figures.push_back(new Polygoneregulier(Couleur{0,1,0}, point{100,200},30,8));
			const Couleur a{0,1,0};
			const point b{100,200};
			_figures.push_back( new Ligne(a,b,point{0,0}));
			_figures.push_back( new Polygoneregulier (a,b,50,5));

			
}
ZoneDessin :: ~ZoneDessin (){
			_figures.clear();
}
bool ZoneDessin:: on_draw(const Cairo::RefPtr<Cairo::Context> & context){
			
		for(unsigned int i=0;i<_figures.size();i++)
				_figures[i]->afficher(context);
		return true;	
	}
