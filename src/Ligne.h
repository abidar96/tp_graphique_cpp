#ifndef LIGNE_H
#define LIGNE_H
#include"figuregeometrique.h"
#include"point.h"
#include"Ligne.h"
#include <gtkmm.h>


class Ligne: public FigureGeometrique
{
    public:
        Ligne(const Couleur& couleur,const point& p0, const point& p1);
        virtual void afficher(const Cairo::RefPtr<Cairo::Context> & context) const override;
        const point& getP0() const;
        const point& getP1() const;


    protected:

    private:
        point _p0;
        point _p1;
};

#endif // LIGNE_H
