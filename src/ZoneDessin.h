#ifndef ZONEDESSIN_H
#define ZONEDESSIN_H
#include <gtkmm.h>
#include <vector>
#include "figuregeometrique.h"

using namespace std;

class ZoneDessin : public Gtk::DrawingArea {
	
	private:
		vector<FigureGeometrique*> _figures;
	public :
		ZoneDessin();
		~ZoneDessin();
	protected:
		bool on_draw(const Cairo::RefPtr<Cairo::Context> & context) override;
		bool gererClic(GdkEventButton event);
	
	
		
};
#endif // ZoneDessin
